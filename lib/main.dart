import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_apps/bloc/movie_bloc.dart';
import 'package:movie_apps/ui/home_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: BlocProvider<MovieBloc>(
        create: (context) => MovieBloc()..add(MovieEvent()),
        child: HomeScreen(),
      ),
    );
  }
}
