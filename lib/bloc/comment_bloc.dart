import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_apps/models/comment.dart';

class CommentEvent {}

abstract class CommentState {}

//ketika load pertama maka class ini yang di return
class CommentUninitialize extends CommentState {}

//ketika data didapatkan maka class ini yang di return
class CommentLoaded extends CommentState {
  List<Comment> comments;
  bool hasReachedMax; //untuk penanda apakah semua data sudah semua terload

  CommentLoaded({this.comments, this.hasReachedMax});

  CommentLoaded copyWith({List<Comment> comments, bool hasReachedMax}) {
    return CommentLoaded(
        comments: comments ?? this.comments,
        hasReachedMax: hasReachedMax ?? this.hasReachedMax);
  }
}

class CommentBloc extends Bloc<CommentEvent, CommentState> {
  CommentBloc() : super(CommentUninitialize());

  @override
  Stream<CommentState> mapEventToState(CommentEvent event) async* {
    List<Comment> comments;
    //jika state awal maka load data awal
    if (state is CommentUninitialize) {
      comments = await Comment.getCommentList(0, 10);
      yield CommentLoaded(comments: comments, hasReachedMax: false);
    } else {
      // jika sudah pernah load data
      CommentLoaded commentLoaded = state as CommentLoaded;
      comments =
          await Comment.getCommentList(commentLoaded.comments.length, 10);
      yield (comments.isEmpty)
          ? commentLoaded.copyWith(hasReachedMax: true)
          : CommentLoaded(
              comments: commentLoaded.comments + comments,
              hasReachedMax: false);
    }
  }
}
