import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_apps/models/movie.dart';

class MovieEvent {}

abstract class MovieState {}

//ketika load pertama maka class ini yang di return
class MovieUninitialize extends MovieState {}

//ketika data didapatkan maka class ini yang di return
class MovieLoaded extends MovieState {
  List<Movie> movies;
  bool hasReachedMax; //untuk penanda apakah semua data sudah semua terload

  MovieLoaded({this.movies, this.hasReachedMax});

  MovieLoaded copyWith({List<Movie> movies, bool hasReachedMax}) {
    return MovieLoaded(
        movies: movies ?? this.movies,
        hasReachedMax: hasReachedMax ?? this.hasReachedMax);
  }
}

class MovieBloc extends Bloc<MovieEvent, MovieState> {
  MovieBloc() : super(MovieUninitialize());
  int initialPage = 1;

  @override
  Stream<MovieState> mapEventToState(MovieEvent event) async* {
    List<Movie> movies;
    //jika state awal maka load data awal
    if (state is MovieUninitialize) {
      try {
        movies = await Movie.getMovieNowPlaying(initialPage);
        yield MovieLoaded(movies: movies, hasReachedMax: false);
      } catch (e) {
        print("error awal $e");
      }
    } else {
      try {
        initialPage++;
        // jika sudah pernah load data
        MovieLoaded movieLoaded = state as MovieLoaded;
        movies = await Movie.getMovieNowPlaying(initialPage);
        yield (movies.isEmpty)
            ? movieLoaded.copyWith(hasReachedMax: true)
            : MovieLoaded(
                movies: movieLoaded.movies + movies, hasReachedMax: false);
      } catch (e) {
        print("error next $e");
      }
    }
  }
}
