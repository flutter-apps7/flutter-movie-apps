import 'package:flutter/material.dart';
import 'package:movie_apps/constants/theme.dart';
import 'package:movie_apps/models/movie.dart';
import 'package:movie_apps/ui/detail_movie/detail_movie.dart';

class MovieCard extends StatelessWidget {
  final Movie movie;

  MovieCard(this.movie);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => DetailMovie(),
          ),
        );
      },
      child: Card(
        elevation: 2,
        child: Container(
          padding: EdgeInsets.all(8),
          child: Row(
            children: [
              ClipRRect(
                child: Container(
                  width: 80,
                  height: 80,
                  child: Stack(
                    children: [
                      Center(
                        child: Image.network(
                          "https://image.tmdb.org/t/p/w500${movie.posterPath}",
                          fit: BoxFit.cover,
                          width: 80,
                          height: 80,
                        ),
                      ),
                      Align(
                        alignment: Alignment.topRight,
                        child: Container(
                          width: 45,
                          height: 30,
                          decoration: BoxDecoration(
                            color: Colors.red.withOpacity(.6),
                            borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(36),
                            ),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                "${movie.voteAverage}/10",
                                style: whiteTextStyle.copyWith(
                                  fontSize: 12,
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      movie.originalTitle,
                      textAlign: TextAlign.left,
                      style: blackTextStyle.copyWith(fontSize: 14),
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    Text.rich(
                      TextSpan(
                        text: "${movie.voteAverage}",
                        style: purpleTextStyle.copyWith(
                          fontSize: 16,
                        ),
                        children: [
                          TextSpan(
                            text: " Rating",
                            style: greyTextStyle.copyWith(
                              fontSize: 16,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Text(
                      "${movie.releaseDate}",
                      style: greyTextStyle,
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
