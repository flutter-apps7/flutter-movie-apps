import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_apps/bloc/movie_bloc.dart';
import 'package:movie_apps/ui/component/movie_card.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  ScrollController controller = ScrollController();
  MovieBloc bloc;

  void onScroll() {
    double maxScroll = controller.position.maxScrollExtent;
    double currentScroll = controller.position.pixels;

    if (currentScroll == maxScroll) bloc.add(MovieEvent());
  }

  @override
  Widget build(BuildContext context) {
    bloc = BlocProvider.of<MovieBloc>(context);
    controller.addListener(onScroll);

    return Scaffold(
      appBar: AppBar(
        title: Text("Movie Apps"),
      ),
      body: Container(
        child: BlocBuilder<MovieBloc, MovieState>(
          builder: (context, state) {
            if (state is MovieUninitialize) {
              return Center(
                child: SizedBox(
                  height: 30,
                  width: 30,
                  child: CircularProgressIndicator(),
                ),
              );
            } else {
              MovieLoaded movieLoaded = state as MovieLoaded;
              return ListView.builder(
                controller: controller,
                itemCount: movieLoaded.movies.length,
                itemBuilder: (context, index) =>
                    (index < movieLoaded.movies.length)
                        ? MovieCard(movieLoaded.movies[index])
                        : Container(
                            child: Center(
                              child: SizedBox(
                                height: 30,
                                width: 30,
                                child: CircularProgressIndicator(),
                              ),
                            ),
                          ),
              );
            }
          },
        ),
      ),
    );
  }
}
