import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:movie_apps/constants/end_point.dart';

class Movie {
  String id;
  String originalTitle;
  String overview;
  String popularity;
  String posterPath;
  String releaseDate;
  String voteAverage;

  Movie(
      {this.id,
      this.originalTitle,
      this.overview,
      this.popularity,
      this.posterPath,
      this.releaseDate,
      this.voteAverage});

  static Future<List<Movie>> getMovieNowPlaying(int page) async {
    var apiResult;
    try {
      Uri apiUrl = Uri.parse(
          "${EndPoint.URL_MOVIE_NOW_PLAYING}?api_key=${EndPoint.API_KEY}&language=en-US&page=$page");
      apiResult = await http.get(apiUrl);
    } catch (exception) {
      print("Error $exception");
    }

    var jsonObject = jsonDecode(apiResult.body)["results"] as List;
    return jsonObject
        .map<Movie>((item) => Movie(
              id: item["id"].toString(),
              originalTitle: item["original_title"],
              overview: item["overview"],
              popularity: item["popularity"].toString(),
              posterPath: item["poster_path"],
              releaseDate: item["release_date"],
              voteAverage: item["vote_average"].toString(),
            ))
        .toList();
  }
}
