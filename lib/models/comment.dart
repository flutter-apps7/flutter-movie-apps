import 'dart:convert';

import 'package:http/http.dart' as http;

class Comment {
  String id;
  String name;
  String email;
  String body;

  Comment({this.id, this.name, this.email, this.body});

  factory Comment.createComment(Map<String, dynamic> object) {
    return Comment(
      id: object["id"].toString(),
      name: object["name"],
      email: object["email"],
      body: object["body"],
    );
  }

  static Future<List<Comment>> getCommentList(int start, int limit) async {
    Uri apiUrl = Uri.parse(
        "https://jsonplaceholder.typicode.com/comments?_start=${start.toString()}&_limit=${limit.toString()}");

    print(
        "https://jsonplaceholder.typicode.com/comments?_start=${start.toString()}&_limit=${limit.toString()}");

    var apiResult = await http.get(apiUrl);
    var jsonObject = jsonDecode(apiResult.body) as List;

    return jsonObject
        .map<Comment>((item) => Comment(
      id: item["id"].toString(),
      name: item["name"],
      email: item["email"],
      body: item["body"],
    ))
        .toList();
  }
}
